/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize(500, 400);
    addAndMakeVisible(colourSelect);
    colourSelect.addMouseListener(this, true);
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    colourSelect.setBounds(0, 0, getWidth(), getHeight()/2);

}

void MainComponent::paint(Graphics &g)
{
    if (x < getWidth() && y < getHeight() && click == true) {
        g.setColour(colourSelect.getCurrentColour());
        g.fillEllipse(x, y, 20, 20);
    }
    
    
    
    
}

void MainComponent::mouseUp(const MouseEvent &event)
{
    DBG("X value = " << event.x);
    DBG("Y value = " << event.y);

    x = event.x;
    y = event.y;
    repaint();
    click = true;
}
